package airline.dao;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import airline.pojo.Seat;

class AirlineDAOImplementationTest {

	AirlineDAOImplementation refAirlineDAOImpl;
	Seat refFirstSeat;
	Seat refEconSeat;
	@BeforeEach
	void setUp() throws Exception {
		refAirlineDAOImpl = new AirlineDAOImplementation();
		refFirstSeat = new Seat(0);
		refEconSeat = new Seat(5);
		
	}

	@AfterEach
	void tearDown() throws Exception {
		refAirlineDAOImpl = null;
	}

	@Test
	void shouldAddNewFirstClassSeat() {
		refAirlineDAOImpl.addNewFirstClassSeat(refFirstSeat);
		assertTrue(refAirlineDAOImpl.seatCapacity[0]);
	}

	@Test
	void shouldAddNewEconClassSeat() {
		refAirlineDAOImpl.addNewEconClassSeat(refEconSeat);
		assertTrue(refAirlineDAOImpl.seatCapacity[5]);
	}

	@Test
	void shouldSeatsAvailable() {
		refAirlineDAOImpl.firstClassSeatNumber = refAirlineDAOImpl.getFirstClassSeatNumber();
		refAirlineDAOImpl.econClassSeatNumber = refAirlineDAOImpl.getEconClassSeatNumber();
		assertTrue(refAirlineDAOImpl.seatsAvailable());
	}
	
	@Test
	void shouldSeatsNotAvailable() {
		refAirlineDAOImpl.firstClassSeatNumber = 5;
		refAirlineDAOImpl.econClassSeatNumber = 10;
		assertFalse(refAirlineDAOImpl.seatsAvailable());
	}

	@Test
	void shouldEconClassSeatsAvailable() {
		assertTrue(refAirlineDAOImpl.econClassSeatsAvailable());
	}
	
	@Test
	void shouldEconClassSeatsNotAvailable() {
		refAirlineDAOImpl.econClassSeatNumber = 10;
		assertFalse(refAirlineDAOImpl.econClassSeatsAvailable());
	}

	@Test
	void shouldFirstClassSeatsAvailable() {
		assertTrue(refAirlineDAOImpl.firstClassSeatsAvailable());
	}
	
	@Test
	void shouldFirstClassSeatsNotAvailable() {
		refAirlineDAOImpl.firstClassSeatNumber = 5;
		assertFalse(refAirlineDAOImpl.firstClassSeatsAvailable());
	}

	@Test
	void shouldGetFirstClassSeatNumber() {
		assertEquals(0, refAirlineDAOImpl.getFirstClassSeatNumber());
	}

	@Test
	void shouldGetEconClassSeatNumber() {
		assertEquals(5, refAirlineDAOImpl.getEconClassSeatNumber());
	}

}
