package airline.dao;

import airline.pojo.Seat;

public interface AirlineDAOInterface {

	void addNewFirstClassSeat(Seat refSeat);
	void addNewEconClassSeat(Seat refSeat);
	boolean seatsAvailable();
	boolean econClassSeatsAvailable();
	boolean firstClassSeatsAvailable();
	int getFirstClassSeatNumber();
	int getEconClassSeatNumber();

}
