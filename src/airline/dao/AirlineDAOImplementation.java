package airline.dao;

import java.util.Arrays;

import airline.pojo.Seat;

public class AirlineDAOImplementation implements AirlineDAOInterface {

	int airlineCapacity = 10;
	boolean[] seatCapacity = new boolean[airlineCapacity];
	int firstClassSeatNumber = 0;
	int econClassSeatNumber = 5;
	

	@Override
	public void addNewFirstClassSeat(Seat refSeat) {		
		seatCapacity[refSeat.getSeatNumber()] = true;	
		System.out.println("\nYour First Class Seat Number: Seat " + (refSeat.getSeatNumber()+1));
		firstClassSeatNumber++;
	}

	@Override
	public void addNewEconClassSeat(Seat refSeat) {
		seatCapacity[refSeat.getSeatNumber()] = true;		
		System.out.println("\nYour Economy Class Seat Number: Seat " + (refSeat.getSeatNumber()+1));
		econClassSeatNumber++;
	}

	@Override
	public boolean seatsAvailable() {
		if (getFirstClassSeatNumber() < 5 || getEconClassSeatNumber() < 10) {
			return true;
		}
		return false;
	}

	@Override
	public boolean econClassSeatsAvailable() {
		if (getEconClassSeatNumber() < 10) {
			return true;
		}
		return false;
	}

	@Override
	public boolean firstClassSeatsAvailable() {
		if (getFirstClassSeatNumber() < 5) {
			return true;
		}
		return false;
	}

	@Override
	public int getFirstClassSeatNumber() {
		return firstClassSeatNumber;
	}

	@Override
	public int getEconClassSeatNumber() {
		return econClassSeatNumber;
	}

}
