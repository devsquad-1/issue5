package airline.service;

import java.util.Scanner;

import airline.dao.AirlineDAOImplementation;
import airline.dao.AirlineDAOInterface;
import airline.pojo.Seat;

public class AirlineServiceImplementation implements AirlineServiceInterface {
	AirlineDAOInterface refAirlineDAO = new AirlineDAOImplementation();;
	Scanner refScanner = new Scanner(System.in);
	int userChoice;
	Seat refSeat;

	@Override
	public void getUserChoice() {
		boolean isRunning = true;
		while (isRunning) {
			userChoice = getUserInput();
			switch (userChoice) {
			case 1:
				addFirstClassSeat();
				break;
			case 2:
				addEconClassSeat();
				break;
			case 3:
				isRunning = false;
				break;
			default:
				System.out.println("\nInvalid Input");
				break;
			}
		}
	}

	@Override
	public void addFirstClassSeat() {
		if (refAirlineDAO.seatsAvailable()) {
			if (refAirlineDAO.firstClassSeatsAvailable()) {
				refSeat = new Seat(refAirlineDAO.getFirstClassSeatNumber());
				refAirlineDAO.addNewFirstClassSeat(refSeat);
			} else {
				System.out.println("\nFirst Class Seats Are Full");
				if (getUserChoiceToSwitchEcon() == 2) {
					addEconClassSeat();
				} else {
					System.out.println("\nNext flight is in 3 hours");
				}
			}
		} else {
			System.out.println("\nThis flight is fully booked");
		}
	}

	@Override
	public void addEconClassSeat() {
		if (refAirlineDAO.seatsAvailable()) {
			if (refAirlineDAO.econClassSeatsAvailable()) {
				refSeat = new Seat(refAirlineDAO.getEconClassSeatNumber());
				refAirlineDAO.addNewEconClassSeat(refSeat);
			} else {
				System.out.println("\nEnconmy Class Seats Are Full");
				System.out.println("\nFirst Class Seats Are Full");
				if (getUserChoiceToSwitchFirst() == 1) {
					addFirstClassSeat();
				} else {
					System.out.println("\nNext flight is in 3 hours");
				}
			}
		} else {
			System.out.println("\nThis flight is fully booked");
		}
	}

	private int getUserInput() {
		System.out.println("\nType 1 for First Class");
		System.out.println("Type 2 for Economy Class");
		System.out.println("Type 3 to exit program");
		System.out.print("Enter your choice: ");
		return refScanner.nextInt();
	}

	private int getUserChoiceToSwitchEcon() {
		System.out.print("\nType 2 to change to Economy Class: ");
		return refScanner.nextInt();
	}

	private int getUserChoiceToSwitchFirst() {
		System.out.print("\nType 1 to change to First Class: ");
		return refScanner.nextInt();
	}

}
