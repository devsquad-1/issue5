package airline.service;

public interface AirlineServiceInterface {

	void getUserChoice();
	void addFirstClassSeat();
	void addEconClassSeat();

}
