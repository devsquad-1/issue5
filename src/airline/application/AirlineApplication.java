package airline.application;

import airline.controller.AirlineController;

public class AirlineApplication {

	public static void main(String[] args) {

		AirlineController refAirlineController = new AirlineController();
		refAirlineController.start();
	}

}