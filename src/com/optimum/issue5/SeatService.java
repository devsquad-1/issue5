package com.optimum.issue5;

import java.util.Scanner;

public class SeatService {

	boolean[] seats = new boolean[10];
	int currentFirstClassSeat = 0;
	int currentEconmonyClassSeat = 5;
	String userChoice;
	Scanner refScanner = new Scanner(System.in);
	
	public String getUserChoice() {
		return userChoice;
	}

	public String setUserChoice() {
		userChoice = refScanner.nextLine().substring(0, 1);
		return userChoice;
	}


	public String promptUser() {
		System.out.println("\nPlease type 1 for First Class");
		System.out.println("Please type 2 for Economy Class");
		System.out.println("Please type 3 to exit program");
		System.out.print("Enter your choice: ");
		return setUserChoice();
	}

	public String promptUserToTakeEcon() {
		System.out.print("\nWould you like to get an Economy Seat?(Y/N) ");
		return setUserChoice();
	}

	public String promptUserToTakeFirstClass() {
		System.out.print("\nWould you like to get a First Class Seat?(Y/N) ");
		return setUserChoice();
	}

	public void runProgram() {
		boolean isRunning = true;
		while (isRunning) {
			promptUser();
			switch (userChoice) {
			case "1":
				addNewFirstClassSeat();
				break;
			case "2":
				addNewEconClassSeat();
				break;
			case "3":
				isRunning = false;
				break;
			default:
				System.out.println("\nIvalid Choice Selected");
				break;
			}
		}
	}

	public void addNewFirstClassSeat() {
		if (checkSeatAvailable()) {
			if (checkFirstClassSeatAvailability()) {
				seats[getCurrentFirstClassSeat()] = true;
				System.out.println("\nYou are in First Class Seat: Seat " + (getCurrentFirstClassSeat()+1));
				currentFirstClassSeat++;
			} else {
				System.out.println("\nFirst Class Seats are fully booked in this flight");
				if (promptUserToTakeEcon().equalsIgnoreCase("Y")) {
					addNewEconClassSeat();
				} else {
					System.out.println("\nNext Flight is in 3 hours");
				}
			}
		} else {
			System.out.println("\nSeat are not available in this flight");
		}

	}

	public void addNewEconClassSeat() {

		if (checkSeatAvailable()) {
			if (checkEconClassSeatAvailability()) {
				seats[getCurrentEconClassSeat()] = true;
				System.out.println("\nYou are in Econmony Class Seat: Seat " + (getCurrentEconClassSeat()+1));
				currentEconmonyClassSeat++;
			} else {
				System.out.println("\nEconmony Class Seats are fully booked in this flight");
				if (promptUserToTakeFirstClass().equalsIgnoreCase("Y")) {
					addNewFirstClassSeat();
				} else {
					System.out.println("\nNext Flight is in 3 hours");
				}
			}
		} else {
			System.out.println("\nSeat are not available in this flight");
		}

	}

	private boolean checkFirstClassSeatAvailability() {
		if (getCurrentFirstClassSeat() < 5 ){
			return true;
		}
		return false;
	}

	private boolean checkEconClassSeatAvailability() {
		if (getCurrentEconClassSeat() < 10) {
			return true;
		}
		return false;
	}

	private boolean checkSeatAvailable() {
		if (getCurrentFirstClassSeat() < 5 || getCurrentEconClassSeat() < 10) {
			return true;
		}
		return false;
	}

	private int getCurrentFirstClassSeat() {
		return currentFirstClassSeat;

	}

	private int getCurrentEconClassSeat() {
		return currentEconmonyClassSeat;

	}

}
